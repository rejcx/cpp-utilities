#ifndef _ARRAY_UTIL_HPP
#define _ARRAY_UTIL_HPP

#include <cstdlib>
#include <ctime>

#include <fstream>
using namespace std;

#include "StringUtil.hpp"

class ArrayUtil
{
    public:
    template <typename T>
    static void GiveRandomValues( T myArray[], int size, T min, T max );
    template <typename T>
    static void Cout( T myArray[], int size );
    template <typename T>
    static void Output( T myArray[], int size, ofstream& out );
    template <typename T>
    static string ToString( T myArray[], int size );
};


template <typename T>
void ArrayUtil::GiveRandomValues( T myArray[], int size, T min, T max )
{
    srand( time( NULL ) );

    T diff = max - min;

    for ( int i = 0; i < size; i++ )
    {
        myArray[i] = T( rand() % diff + min );
    }
}

template <typename T>
void ArrayUtil::Cout( T myArray[], int size )
{
    for ( int i = 0; i < size; i++ )
    {
        if ( i != 0 ) { cout << ", "; }
        cout << myArray[i];
    }
    cout << endl;
}

template <typename T>
void ArrayUtil::Output( T myArray[], int size, ofstream& out )
{
    for ( int i = 0; i < size; i++ )
    {
        if ( i != 0 ) { cout << ", "; }
        out << myArray[i];
    }
    out << endl;
}

template <typename T>
string ArrayUtil::ToString( T myArray[], int size )
{
    string text = "";

    for ( int i = 0; i < size; i++ )
    {
        if ( i != 0 ) { text += ", "; }
        text += StringUtil::ToString( myArray[i] );
    }

    return text;
}


#endif
