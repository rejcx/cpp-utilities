#include <iostream>
#include <climits>
using namespace std;

#include "Timer.hpp"

void CountUp( int start, int end )
{
    for ( int i = start; i < end; i++ )
    {
        cout << i << "...";
    }
}

int main()
{
    Timer timer;

    timer.Start();
    CountUp( 0, 1000000 );
    
    cout << endl << endl << timer.GetElapsedSeconds() << " seconds" << endl;
    cout << endl << endl << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
    
    return 0;
}
